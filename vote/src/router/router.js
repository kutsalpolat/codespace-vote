/**
 * Renderer - vue
 *
 * @process: RENDERER PROCESS
 */



import Home from "../page/Home.vue";
import Add from "../page/New.vue";





module.exports = (Router) => {

  /////////////////////////
  // Route Settings
  /////////////////////////

  const routes = [

    {path: "/", component: Home, name: "home"},
    {path: "/add", component: Add, name: "add"},


  ];

  return new Router({
    mode: 'history',
    routes,
    history: false,
    scrollBehavior: () => {
      return {y: 0};
    }
  });
};


