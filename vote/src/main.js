
import Vue from 'vue'
import Router from "vue-router";
import Vuex from 'vuex'
import App from './App.vue'
import store from './store/store.js'



require('bulma/bulma.sass');


//CSS
require('./assets/static/css/main.css');


Vue.use(Router);
Vue.use(Vuex);


// Set Store Data in LocalStore


if (!localStorage.allList) {
  localStorage.setItem('allList', JSON.stringify(store.state.allList));
} else {
  store.dispatch("CHANGE_STORE_DATA", JSON.parse(localStorage.allList));
}


if(store.state.rateList.length === 0) {
  if(localStorage.rateList) {
    store.dispatch("CHANGE_RATE_LIST");
  }
}



function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

if(!localStorage.userId) {
  localStorage.setItem('userId', guid());
}


Date.prototype.yyyymmdd = function() {
  let mm = this.getMonth() + 1; // getMonth() is zero-based
  let dd = this.getDate();

  return [this.getFullYear(),
    (mm>9 ? '' : '0') + mm,
    (dd>9 ? '' : '0') + dd
  ].join('/');
};

Array.prototype.remove = function(from, to) {
  let rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

const router = require("./router/router")(Router);

//import router from "./routes/routes";
//require('./assets/static/js/main.js');



new Vue({
  el: '#app',
  store,
  router,

  render: h => h(App)
});

