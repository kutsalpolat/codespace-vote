import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);



let state = {
  allList: [
    {
      id:"269d0ad7-a22a-47e5-ad50-7e2217cef8a6",
      title:"Google.com",
      website:"https://www.google.com.tr",
      count:10,
      date:"2012-04-23T18:25:43.511Z",
    },
    {
      id:"a976ebbb-0bfb-4ebb-b004-0e89ab93f0eb",
      title:"Mystalk",
      website:"https://www.mystalk.com",
      count:6,
      date:"2013-02-23T18:25:43.511Z",

    },
    {
      id:"38b3e56f-1664-459d-903a-c70863d1ebb5",
      title:"Facebook",
      website:"https://www.facebook.com",
      count:14,
      date:"2011-07-23T18:25:43.511Z",
    },
    {
      id:"4b15c8c2-aa73-4f8b-bed6-df6905b9ebbe",
      title:"Django",
      website:"https://www.django.com",
      count:2,
      date:"2015-06-23T18:22:43.511Z",
    },
    {
      id:"f9ca9527-29b0-4099-a690-d48222f31a22",
      title:"Twitter",
      website:"https://www.twitter.com",
      count:15,
      date:"2017-04-23T18:25:43.511Z",
    },
    {
      id:"31e5939d-2aca-44db-9a60-38800851d79d",
      title:"StackOverFlow",
      website:"https://www.stackoverflow.com",
      count:1,
      date:"2018-02-23T18:25:43.511Z",
    },
    // Set

    {
      id:"5cd6bb62-ea25-4d83-a328-9ecdf9aedff6",
      title:"Markafoni",
      website:"https://www.markafoni.com",
      count:32,
      date:"2014-05-23T18:25:43.511Z",
    },
    {
      id:"9eda738f-10e8-450c-8e84-3f16438e7961",
      title:"Linkedin",
      website:"https://www.linkedin.com",
      count:16,
      date:"2010-04-23T18:25:43.511Z",
    },
    {
      id:"b0aaa50c-c4d2-4921-9fd1-e4fb186754a0",
      title:"Sketch",
      website:"https://www.sketchapp.com",
      count:11,
      date:"2016-01-23T18:25:43.511Z",
    },
    {
      id:"3d0a5a6a-f272-4fac-83f7-bca632ee8596",
      title:"Hepsiburada",
      website:"https://www.hepsiburada.com",
      count:8,
      date:"2012-04-20T18:25:43.511Z",
    },
    {
      id:"d280cb09-72a1-4316-aa96-1e0c6ec6ca2f",
      title:"Ruby",
      website:"https://www.ruby.com",
      count:14,
      date:"2018-03-23T18:25:43.511Z",
    },
    {
      id:"0add7dc0-5921-458d-a4bc-b9e4e3622571",
      title:"Apple",
      website:"https://www.apple.com",
      count:20,
      date:"2016-04-23T18:25:43.511Z",
    },


  ],
  rateList: []
}



let mutations = {
  UPDATE_ALL_LIST_PLUS (state, content) {

    let rateControl = state.rateList.filter((v) => {
      if(v.id === content.id) {
        return v
      }
    })[0];

    state.allList.filter((v) => {

      if(v.id === content.id) {

        if (content.rate === 'up' && rateControl && rateControl.rate === 'down') {
          v.count = v.count + 2
        } else if (content.rate === 'down' && rateControl && rateControl.rate === 'up') {
          v.count = v.count - 2
        } else if (content.rate === 'down' && rateControl && rateControl.rate === 'down') {
          v.count = v.count + 1
        } else if (content.rate === 'up' && rateControl && rateControl.rate === 'up') {
          v.count = v.count - 1
        } else if (content.rate === 'down') {
          v.count = v.count - 1
        } else {
          v.count = v.count + 1
        }

      }

    });

    localStorage.setItem('allList', JSON.stringify(state.allList));

  },
  UPDATE_STORE_DATA (state, content) {
    state.allList = content
  },
  UPDATE_RATE (state, content) {

    let getRateList = state.rateList;

    let itemFormat = {
      id:content.id,
      rate:content.rate,
      date:Date.now()
    }
    let item = state.rateList.filter((v) => {
      return v.id === content.id ? v:''
    });

    if (item.length === 0) {
      getRateList.push(itemFormat)
    } else {
      if (item[0].rate === content.rate) {
        getRateList = getRateList.filter((v) => {
          return v.id !== content.id;
        });
      } else {
        item[0].rate = content.rate;
        item[0].date = Date.now();
      }
    }
    state.rateList = getRateList;

    localStorage.setItem('rateList', JSON.stringify(state.rateList));
  },
  UPDATE_RATE_LIST (state) {
    state.rateList = JSON.parse(localStorage.rateList);
  },
  UPDATE_ADD_LIST (state, content) {
    state.allList.push(content)

    localStorage.setItem('allList', JSON.stringify(state.allList));

  },


}


const actions = {
  CHANGE_ALL_LIST_PLUS({commit, state},content) {
    commit("UPDATE_ALL_LIST_PLUS",content);
  },
  CHANGE_STORE_DATA({commit, state},content) {
    commit("UPDATE_STORE_DATA",content);
  },
  CHANGE_RATE({commit, state},content) {
    commit("UPDATE_RATE",content);
  },
  CHANGE_RATE_LIST({commit, state}) {
    commit("UPDATE_RATE_LIST");
  },
  CHANGE_ADD_LIST({commit, state},content) {

    console.log(content)
    commit("UPDATE_ADD_LIST",content);
  },



};





export default new Vuex.Store({
  state,
  mutations,
  actions,
})

